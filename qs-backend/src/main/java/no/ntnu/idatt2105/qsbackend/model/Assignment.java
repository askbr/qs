package no.ntnu.idatt2105.qsbackend.model;

/**
 * The class defines an assignment with the field assignmentNum.
 */
public class Assignment {
    private Integer assignmentNum;

    /** @return Assignment number of this object */
    public Integer getAssignmentNum() {
        return assignmentNum;
    }

    /** Sets the assignment number of this object */
    public void setAssignmentNum(Integer assignmentNum) {
        this.assignmentNum = assignmentNum;
    }
}

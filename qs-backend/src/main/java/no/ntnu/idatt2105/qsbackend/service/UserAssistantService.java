package no.ntnu.idatt2105.qsbackend.service;

import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.model.request.CourseIdRequest;
import no.ntnu.idatt2105.qsbackend.repository.UserAssistantRepositoryJdbc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class acts as the layer between endpoints (Controller) and database access (Repository).
 * The class handles validation and other business logic.
 */
@Service
public class UserAssistantService {
    @Autowired
    private UserAssistantRepositoryJdbc repository;

    Logger logger = LoggerFactory.getLogger(UserAssistantService.class);

    /**
     * Gets courses of user assistant from database (using repository)
     *
     * @param userId ID of the user assistant
     * @return List of courses
     */
    public List<Course> getUserAssistantCourses(Integer userId) {
        return repository.findUserAssistantCourses(userId);
    }

    /**
     * Adds user assistant to course in database (using repository)
     *
     * @param userId ID of the user
     * @param courseIdRequest Object containing ID of the course
     * @return Response with request status (success/fail)
     */
    public ResponseEntity<String> addUserAssistantToCourse(Integer userId, CourseIdRequest courseIdRequest) {
        try {
            repository.save(userId, courseIdRequest.getCourseId());

            logger.info("User with userId=" + userId
                    + " was added as assistant to course with courseId="
                    + courseIdRequest.getCourseId());
            return new ResponseEntity<>("User successfully added as assistant to course.", HttpStatus.CREATED);
        } catch (Exception e) {
            logger.warn("Error occurred when adding user with userId=" + userId
                    + " as assistant to course with courseId=" + courseIdRequest.getCourseId()
                    + "\n The error message: \n" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

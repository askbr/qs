package no.ntnu.idatt2105.qsbackend.controller;

import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.model.request.CourseCodeRequest;
import no.ntnu.idatt2105.qsbackend.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Class defines endpoints for managing a Course instance
 */
@CrossOrigin
@RestController
@RequestMapping("/api")
public class CourseController {
    @Autowired
    private CourseService service;

    Logger logger = LoggerFactory.getLogger(CourseController.class);

    /**
     * @return List of all courses
     */
    @GetMapping("/courses")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public List<Course> getAllCourses() {
        logger.debug("Endpoint /courses received a GET request.");
        return service.getAllCourses();
    }

    /**
     * Gets metadata for a course based on courseCode only.
     * (courseCodes are unique  for a course in database)
     *
     * @param request CourseCodeRequest object. (An wrapper for courseCode)
     * @return Course metadata
     */
    @PostMapping("/courses/metadata")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public Course getCourseByCourseCode(@RequestBody CourseCodeRequest request) {
        return service.getCourseByCourseCode(request);
    }

    /**
     * @param courseId ID of course to return from database
     * @return Course of given courseId
     */
    @GetMapping("/courses/{course_id}")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public Course getCourseOfId(@PathVariable(value="course_id") Integer courseId) {
        logger.debug("Endpoint /courses/{course_id} received a GET request.");
        return service.getCourseOfId(courseId);
    }

    /**
     *
     * @param course Course to be added to database
     * @return Response with request status (success/fail)
     */
    @PostMapping("/courses")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<String> addCourse(@RequestBody Course course) {
        logger.debug("Endpoint /courses received a POST request.");
        return service.addCourse(course);
    }

    /**
     * Updates a course
     *
     * @param course The new value for course, fields that is null will not be updated
     * @param courseId ID of course to be updated
     *
     * @return Response with request status (success/fail)
     */
    @PutMapping("/courses/{course_id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<String> updateCourse(@RequestBody Course course,
                                              @PathVariable(value="course_id") Integer courseId) {
        logger.debug("Endpoint /courses/{course_id} received a PUT request.");
        return service.updateCourse(courseId, course);
    }

    /**
     * Deletes a course
     *
     * @param courseId ID of course to be deleted
     * @return Response with request status (success/fail)
     */
    @DeleteMapping("/courses/{course_id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<String> deleteCourse(@PathVariable(value="course_id") Integer courseId) {
        logger.debug("Endpoint /courses/{course_id} received a DELETE request.");
        return service.deleteCourseById(courseId);
    }
}

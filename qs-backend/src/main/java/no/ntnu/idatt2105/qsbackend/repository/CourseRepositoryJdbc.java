package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is an implementation of CourseRepository using JDBC.
 * See CourseRepository for javadoc.
 */
@Repository
public class CourseRepositoryJdbc implements CourseRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Course> findAll() {
        String query = "SELECT * FROM course";

        return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Course.class));
    }

    @Override
    public Course findById(Integer courseId) {
        String query = "SELECT * FROM course WHERE course_id=?";

        try {
            Course course = jdbcTemplate.queryForObject(query,
                    BeanPropertyRowMapper.newInstance(Course.class), courseId);
            return course;
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }

    @Override
    public int save(Course course) {
        String query = "INSERT INTO " +
                "course (course_id,course_name,course_code,num_of_assignments,num_of_required_assignments,is_queue_open)" +
                "VALUES (DEFAULT,?,?,?,?,?)";

        return jdbcTemplate.update(query,
                course.getCourseName(),
                course.getCourseCode(),
                course.getNumOfAssignments(),
                course.getNumOfRequiredAssignments(),
                course.getIsQueueOpen());
    }

    @Override
    public int deleteById(Integer courseId) {
        String query = "DELETE FROM course WHERE course_id=?";
        return jdbcTemplate.update(query, courseId);
    }

    @Override
    public int update(Course course) {
        String query = "UPDATE course " +
                "SET course_name=?,course_code=?,num_of_assignments=?,num_of_required_assignments=?, is_queue_open=? " +
                "WHERE course_id=?";

        return jdbcTemplate.update(query,
                course.getCourseName(),
                course.getCourseCode(),
                course.getNumOfAssignments(),
                course.getNumOfRequiredAssignments(),
                course.getIsQueueOpen(),
                course.getCourseId());
    }

    @Override
    public Course findByCode(String courseCode) {
        String query = "SELECT * FROM course WHERE course_code=?";

        try {
            Course course = jdbcTemplate.queryForObject(query,
                    BeanPropertyRowMapper.newInstance(Course.class), courseCode);
            return course;
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }
}

package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.ApplicationUser;
import no.ntnu.idatt2105.qsbackend.model.BasicApplicationUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static no.ntnu.idatt2105.qsbackend.security.role.ApplicationUserRole.*;


@Repository
public class ApplicationUserRepositoryJdbc implements ApplicationUserRepository {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ApplicationUserRepositoryJdbc(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {
        return getApplicationUsers()
                .stream()
                .filter(applicationUser -> username.equals(applicationUser.getUsername()))
                .findFirst();
    }

    private List<ApplicationUser> getApplicationUsers() {
        String q = "SELECT email, password, role FROM user";
        List<BasicApplicationUser> users = jdbcTemplate.query(
                q,
                BeanPropertyRowMapper.newInstance(BasicApplicationUser.class)
        );
        List<ApplicationUser> applicationUsers = new ArrayList<>();
        for (BasicApplicationUser u : users) {
            applicationUsers.add(
                    new ApplicationUser(
                            u.getEmail(),
                            passwordEncoder.encode(u.getPassword()),
                            (u.getRole().equals("ADMIN")) ? ADMIN.getGrantedAuthorities() : USER.getGrantedAuthorities(),
                            true,
                            true,
                            true,
                            true
                    )
            );
        }
        return applicationUsers;
    }

}

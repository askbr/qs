package no.ntnu.idatt2105.qsbackend.controller;

import no.ntnu.idatt2105.qsbackend.model.Assignment;
import no.ntnu.idatt2105.qsbackend.service.UserAssignmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Class defines endpoints for managing assignments for a user
 */
@CrossOrigin
@RestController
@RequestMapping("/api")
public class UserAssignmentController {

    @Autowired
    private UserAssignmentService service;

    Logger logger = LoggerFactory.getLogger(UserAssignmentController.class);

    /**
     * Approves a course assignment for user
     *
     * @param userId ID of user to approve an assignment for
     * @param courseId ID of course user should get approved status for an assignment
     * @param assignment Object with assignmentNum property. Defines which assignment to approve
     * @return Response with request status (success/fail)
     */
    @PostMapping("/users/{user_id}/courses/{course_id}/assignments")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public ResponseEntity<String> approveAssignmentForUser(@PathVariable(value="user_id") Integer userId,
                                                           @PathVariable(value="course_id") Integer courseId,
                                                           @RequestBody Assignment assignment) {
        logger.debug("Endpoint /users/{user_id}/courses/{course_id}/assignments received a POST request.");
        return service.approveAssignmentForUser(userId, courseId, assignment);
    }

    /**
     * Gets approved assignments for a user in a specific course
     * @param userId ID of the user
     * @param courseId ID of the course
     * @return List of assignments completed
     */
    @GetMapping("/users/{user_id}/courses/{course_id}/assignments")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<Assignment> getApprovedAssignmentsForUserInCourse(@PathVariable(value="user_id") Integer userId,
                                                          @PathVariable(value="course_id") Integer courseId) {
        logger.debug("Endpoint /users/{user_id}/courses/{course_id}/assignments received a GET request.");
        return service.getApprovedAssignmentsForUserInCourse(userId, courseId);
    }

}

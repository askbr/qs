package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.UserCourseQueueData;

import java.util.List;

/**
 * The UserCourseQueueRepository interface is implemented by classes that modifies database data
 * related to Users In Queue for a specific Course. The specific operations are specified by the
 * methods of this class. Modification can be done by using JDBC or JPA or other tools that
 * connect Java to Database.
 */
public interface UserCourseQueueRepository {
    /**
     * Finds all the courses a user is in queue for
     *
     * @param userId ID of the user
     * @return Object with data describing the user-queue relationship in the database
     */
    List<UserCourseQueueData> findAllCoursesUserIsInQueueFor(Integer userId);

    /**
     * Finds all the users a user in queue for a course
     *
     * @param courseId ID of the course
     * @return Object with data describing the user-course queue relationship in the database
     */
    List<UserCourseQueueData> findAllUsersInQueueForCourse(Integer courseId);

    /**
     * Saves a user-course queue entry in the database
     *
     * @param data Data describing the user-course queue relationship
     * @return The number of rows affected
     */
    int save(UserCourseQueueData data);

    /**
     * Deletes a user-course queue entry from the database
     * @param userId ID of the user
     * @param courseId ID of the course
     * @return The number of rows affected
     */
    int deleteByIds(Integer userId, Integer courseId);

    /**
     * Finds user-course queue entry by specified user ID and course ID from database
     *
     * @param userId ID of the user to find
     * @param courseId ID of the course to find
     * @return The course (if found)
     */
    UserCourseQueueData findByIds(Integer userId, Integer courseId);

    /**
     * Updates a user queue entry
     *
     * @param data The new data for user-queue entry (replaces the old data)
     * @return The number of rows affected in the database
     */
    int update(UserCourseQueueData data);
}

package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * This class is an implementation of UserRepository using JDBC.
 * See UserRepository for javadoc.
 */
@Repository
public class UserRepositoryJdbc implements UserRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public User findByEmail(String email) {
        String query = "SELECT * FROM user WHERE email=?";

        try {
            User user = jdbcTemplate.queryForObject(query,
                    BeanPropertyRowMapper.newInstance(User.class), email);
            return user;
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }

    @Override
    public User findById(Integer userId) {
        String query = "SELECT * FROM user WHERE user_id=?";

        try {
            User user = jdbcTemplate.queryForObject(query,
                    BeanPropertyRowMapper.newInstance(User.class), userId);
            return user;
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }

    @Override
    public int save(User user) {
        String query = "INSERT INTO " +
                "user (user_id,first_name,last_name,email,password,role)" +
                "VALUES (DEFAULT,?,?,?,?,?)";

        return jdbcTemplate.update(query,
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                user.getRole());
    }
}

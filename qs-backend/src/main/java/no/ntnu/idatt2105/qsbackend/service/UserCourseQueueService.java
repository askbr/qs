package no.ntnu.idatt2105.qsbackend.service;

import no.ntnu.idatt2105.qsbackend.model.User;
import no.ntnu.idatt2105.qsbackend.model.UserCourseQueueData;
import no.ntnu.idatt2105.qsbackend.repository.UserCourseQueueRepositoryJdbc;
import no.ntnu.idatt2105.qsbackend.security.jwt.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class acts as the layer between endpoints (Controller) and database access (Repository).
 * The class handles validation and other business logic.
 */
@Service
public class UserCourseQueueService {
    @Autowired
    UserCourseQueueRepositoryJdbc repository;

    @Autowired
    private JwtUtils jwtUtils;

    Logger logger = LoggerFactory.getLogger(UserCourseQueueService.class);

    /**
     * Gets data of courses a user currently is in queue for to get help/approve an assignment
     * from database (using repository)
     *
     * @param userId ID of the user
     * @return List of courses a user currently is in queue for, each list entry includes data
     * for the user-course-queue relationship
     */
    public List<UserCourseQueueData> getCoursesUserIsInQueueFor(Integer userId) {
        return repository.findAllCoursesUserIsInQueueFor(userId);
    }

    /**
     * Gets data of courses a user currently is in queue for to get help/approve an assignment
     *
     * @param courseId ID of the course
     * @return List of users a course currently has in its queue. Each list entry includes data
     * for the user-course-queue relationship
     */
    public List<UserCourseQueueData> getUsersInQueueForCourse(Integer courseId) {
        return repository.findAllUsersInQueueForCourse(courseId);
    }

    /**
     * Adds user in course queue with data about the queue entry.
     * The data is saved in the database (using repository)
     *
     * @param courseId ID of the course
     * @param data Initial data for the user-course-queue relationship. The courseId field of this
     *             object is ignored.
     *
     * @return Status message and code, based on if repository could add user to queue in course
     */
    public ResponseEntity<String> addUserToQueueInCourse(Integer courseId, UserCourseQueueData data, String token) {
        try {
            data.setCourseId(courseId); // Ignoring the field of object, parameter courseId is used

            // Check if user who is trying to add is the user itself or a ADMIN
            User tokenUser = jwtUtils.getUserOfToken(token);
            if (!tokenUser.getRole().equals("ADMIN") && !tokenUser.getUserId().equals(data.getUserId())) {
                return new ResponseEntity<>("userId of token (" + tokenUser.getUserId() + ")"
                        + " does not match userId of request (" + data.getUserId() + ")", HttpStatus.FORBIDDEN);
            }

            // Validating data
            if (isUserCourseQueueDataValid(data)) {
                repository.save(data);
            } else {
                logger.warn("User queue status for course attempted added with invalid values:\n" + data.toString());
                return new ResponseEntity<>("The users queue data for the course is invalid",
                        HttpStatus.BAD_REQUEST);
            }

            logger.info("User added to queue for course with courseId=" + courseId
                    + " with the queue data:\n" + data.toString());
            return new ResponseEntity<>("User was added to course queue successfully", HttpStatus.CREATED);
        } catch (Exception e) {
            logger.warn("User could not be added to queue in course with courseId=" + courseId
                    + ". Queue data:\n" + data.toString() + "\n Error message: \n" + e.getMessage());
            return new ResponseEntity<>("Could not add user to course queue", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Removes user from the queue of a course in the database (using repository)
     *
     * @param userId ID of the user
     * @param courseId ID of the course
     * @return Status message and code, based on if repository could remove user from queue in course
     */
    public ResponseEntity<String> removeUserFromQueueInCourse(Integer userId, Integer courseId) {
        try {
            int result = repository.deleteByIds(userId, courseId);

            if (result == 0) { // No rows affected
                logger.info("User with id=" + userId
                        + " was not found when trying to remove user from queue in course with courseId="
                        + courseId);
                return new ResponseEntity<>("User with id=" + userId
                        + " and courseId=" + courseId +" was not found", HttpStatus.NOT_FOUND);
            }
            logger.info("User with userId=" + userId
                    + " removed from queue in course with courseId=" + courseId);
            return new ResponseEntity<>("User is no longer in queue for given course", HttpStatus.OK);

        } catch (Exception e) {
            logger.warn("User with userId=" + userId
                    + " could not be removed from course with courseId=" + courseId
                    + "\n Error message: \n" + e.getMessage());
            return new ResponseEntity<>("Could not delete user from course queue",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Updates the queue data for a user in a course. This is done in the database, using repository.
     *
     * @param userId ID of the user
     * @param courseId ID of the course
     * @param data The updated queue data for a user in a course. The user ID and course ID properties of
     *             this object will be ignored. The userId and courseId params will be used to locate
     *             database entry.
     *             If a field of the data object is null it will stay unchanged in the database
     * @return Status message and code, based on if repository could update user-course-queue relationship
     */
    public ResponseEntity<String> updateQueueStateForUserInCourse(Integer userId, Integer courseId, UserCourseQueueData data) {

        /*
         Check for what user is trying to update a queue entry should be added.
         Only student assistants, the user itself and admins should be able to change a users
         queue entry.

         Possible solution: Add USER_ASSISTANT role
         */

        UserCourseQueueData d = repository.findByIds(userId, courseId);
        if (d != null) {
            d.setCourseId(courseId);
            d.setUserId(userId);
            if (data.getAssignmentNum() != null) d.setAssignmentNum(data.getAssignmentNum());
            if (data.getEnqueueType() != null) d.setEnqueueType(data.getEnqueueType());
            if (data.getLocation() != null) d.setLocation(data.getLocation());
            if (data.getIsGettingAssistance() != null) d.setIsGettingAssistance(data.getIsGettingAssistance());

            if (isUserCourseQueueDataValid(d)) {
                repository.update(d);
            } else {
                logger.warn("User queue status for course attempted updated to invalid values:\n" + d.toString());
                return new ResponseEntity<>("The users queue data for the course is invalid",
                        HttpStatus.BAD_REQUEST);
            }

            logger.info("Updated queue for course with courseId=" + courseId
                    + " and user with userId="+ userId + ". New data: \n" + d.toString());
            return new ResponseEntity<>("User state in course queue was updated", HttpStatus.OK);
        } else {
            logger.info("Could not find queue entry for user with userId=" + userId
                    + " for course with courseId=" + courseId);
            return new ResponseEntity<>("Could not find user entry in the course", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets queue info for a user in queue for a course
     *
     * @param userId ID of the suer
     * @param courseId ID of the course
     * @return The queue info
     */
    public UserCourseQueueData getQueueInfoForUserInCourse(Integer userId, Integer courseId) {
        UserCourseQueueData data = repository.findByIds(userId, courseId);
        if (data == null) logger.info("Queue data not found for userId=" + userId
                + " and courseId=" + courseId);
        return data;
    }

    private boolean isUserCourseQueueDataValid(UserCourseQueueData data) {
        if (data.getEnqueueType() == null) return false;
        if (data.getAssignmentNum() == null || data.getAssignmentNum() <= 0) return false;
        if (data.getLocation() == null || data.getLocation().isBlank()) return false;
        return true;
    }
}

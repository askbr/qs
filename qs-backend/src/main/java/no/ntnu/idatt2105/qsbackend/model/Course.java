package no.ntnu.idatt2105.qsbackend.model;

/**
 * The class defines a course with the fields courseId, courseName, courseCode, numOfAssignments,
 * numOfRequiredAssignments and isQueueOpen.
 */
public class Course {
    private Integer courseId;
    private String courseName;
    private String courseCode;
    private Integer numOfAssignments; // Total number of assignments in the course
    private Integer numOfRequiredAssignments; // Number of required assignments in the course
    private Boolean isQueueOpen; // True: queue is open, False: queue is closed

    /** Empty Constructor. Requirement of Spring Boot.*/
    public Course() {}

    /** Constructor with all fields */
    public Course(Integer courseId, String courseName, String courseCode, Integer numOfAssignments,
                  Integer numOfRequiredAssignments, Boolean isQueueOpen) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseCode = courseCode;
        this.numOfAssignments = numOfAssignments;
        this.numOfRequiredAssignments = numOfRequiredAssignments;
        this.isQueueOpen = isQueueOpen;
    }

    /** @return ID of this course object */
    public Integer getCourseId() {
        return courseId;
    }

    /** Sets the course ID this course object */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /** @return Name of this course object */
    public String getCourseName() {
        return courseName;
    }

    /** Sets the course name this course object */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /** @return Code of this course object */
    public String getCourseCode() {
        return courseCode;
    }

    /** Sets the course code this course object */
    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    /** @return Number of assignments in the course of this object */
    public Integer getNumOfAssignments() {
        return numOfAssignments;
    }

    /** Sets the number of assignments of this course object */
    public void setNumOfAssignments(Integer numOfAssignments) {
        this.numOfAssignments = numOfAssignments;
    }

    /** @return Number of required assignments in the course of this object */
    public Integer getNumOfRequiredAssignments() {
        return numOfRequiredAssignments;
    }

    /** Sets the number of required assignments of this course object */
    public void setNumOfRequiredAssignments(Integer numOfRequiredAssignments) {
        this.numOfRequiredAssignments = numOfRequiredAssignments;
    }

    /** @return true: queue is open. false: queue is closed */
    public Boolean getIsQueueOpen() {
        return isQueueOpen;
    }

    /** Sets the queue status to open/closed of this course object */
    public void setIsQueueOpen(Boolean queueOpen) {
        isQueueOpen = queueOpen;
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", courseCode='" + courseCode + '\'' +
                ", numOfAssignments=" + numOfAssignments +
                ", numOfRequiredAssignments=" + numOfRequiredAssignments +
                ", isQueueOpen=" + isQueueOpen +
                '}';
    }
}

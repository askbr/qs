package no.ntnu.idatt2105.qsbackend.service;

import no.ntnu.idatt2105.qsbackend.model.Assignment;
import no.ntnu.idatt2105.qsbackend.repository.UserAssignmentRepositoryJdbc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class acts as the layer between endpoints (Controller) and database access (Repository).
 * The class handles validation and other business logic.
 */
@Service
public class UserAssignmentService {

    Logger logger = LoggerFactory.getLogger(UserAssignmentService.class);

    @Autowired
    private UserAssignmentRepositoryJdbc repository;

    /**
     * Approves an assignment for a user in a course using repository.
     *
     * @param userId ID of user
     * @param courseId ID of course
     * @param assignment Assignment to imporve, identified by its assignment number field
     * @return Status message and code, based on if repository could approve assignment in database
     */
    public ResponseEntity<String> approveAssignmentForUser(Integer userId,
                                                           Integer courseId,
                                                           Assignment assignment) {
        try {
            repository.save(userId, courseId, assignment.getAssignmentNum());

            logger.info("Assignment with assignmentNum=" + assignment.getAssignmentNum()
                    + " was marked completed for user with userId=" + userId
                    + " in course with courseId=" + courseId);
            return new ResponseEntity<>("Assignment successfully added to completed", HttpStatus.CREATED);
        } catch (Exception e) {
            logger.warn("Could not mark assignmentNum=" + assignment.getAssignmentNum()
                    + " as completed for user with userId=" + userId
                    + " in course with courseId=" + courseId);
            return new ResponseEntity<>("Assignment could not be set to complete for user",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Gets all approved assignments for user in a course
     *
     * @param userId ID of the user
     * @param courseId ID of the course
     * @return List of the approved assignments
     */
    public List<Assignment> getApprovedAssignmentsForUserInCourse(Integer userId, Integer courseId) {
        return repository.findAllByIds(userId, courseId);
    }
}

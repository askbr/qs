package no.ntnu.idatt2105.qsbackend.model.request;

/**
 * The class represents a request-body when sending a course id to this Spring Boot Application.
 *
 */
public class CourseIdRequest {
    private Integer courseId;

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }
}

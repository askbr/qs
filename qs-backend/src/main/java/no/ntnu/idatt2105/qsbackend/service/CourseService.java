package no.ntnu.idatt2105.qsbackend.service;


import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.model.User;
import no.ntnu.idatt2105.qsbackend.model.request.CourseCodeRequest;
import no.ntnu.idatt2105.qsbackend.repository.CourseRepositoryJdbc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class acts as the layer between endpoints (Controller) and database access (Repository).
 * The class handles validation and other business logic.
 */
@Service
public class CourseService {

    Logger logger = LoggerFactory.getLogger(CourseService.class);

    @Autowired
    private CourseRepositoryJdbc repository;

    /**
     * Gets all courses from repository
     *
     * @return List of courses
     */
    public List<Course> getAllCourses() {
        return repository.findAll();
    }

    /**
     * Gets a course by ID from repository
     *
     * @param courseId ID of the course
     * @return The course found by repository
     */
    public Course getCourseOfId(Integer courseId) {
        Course course = repository.findById(courseId);
        if (course == null) logger.warn("Course with courseId=" + courseId + " was not found.");
        return course;
    }

    // DOCUMENT!

    /**
     * Gets a user based on courseCode from repository.
     *
     * @param request Object containing the course code
     * @return Course from repository (if found. Can be null.)
     */
    public Course getCourseByCourseCode(CourseCodeRequest request) {
        Course course = repository.findByCode(request.getCourseCode());

        if (course == null) logger.info("User with courseCode=" + request.getCourseCode()
                + " was not found.");
        return course;
    }

    /**
     * Adds course to database (using repository). All courses are initialized with a closed queue.
     * The course is validated by the isCourseValid method before its sent onward to repository.
     *
     * @param course Course to add, ID will be ignored and set by database
     * @return Status message and code, based on if repository could add the course to database
     */
    public ResponseEntity<String> addCourse(Course course) {
        // Checking if the course code is unique
        Course c = repository.findByCode(course.getCourseCode());
        if (c != null) { // course with courseCode already exists
            logger.warn("Course attempted stored in database with already existing course code:\n"
                    + course.toString());
            return new ResponseEntity<>("There already exists a course with course code: "
                    + course.getCourseCode(), HttpStatus.BAD_REQUEST);
        }

        try {
            course.setIsQueueOpen(false); // All courses will have a closed queue initially

            // Only saving if the course data is valid
            if (isCourseValid(course)) {
                repository.save(course);
            } else {
                logger.warn("Course attempted stored in database with invalid values:\n" + course.toString());
                return new ResponseEntity<>("Course is invalid", HttpStatus.BAD_REQUEST);
            }

            logger.info("Course was stored:\n" + course.toString());
            return new ResponseEntity<>("Course was successfully stored", HttpStatus.CREATED);
        } catch (Exception e) {
            logger.warn("Error occurred when adding a new course: \n" + course.toString() +
                    "\n The error message: \n" + e.getMessage());

            return new ResponseEntity<>("Error occurred when trying to add course to database",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Deletes course from database (using repository)
     *
     * @param courseId ID of the course to delete
     * @return Status message and code, based on if repository could delete the course from database
     */
    public ResponseEntity<String> deleteCourseById(Integer courseId) {
        try {
            int result = repository.deleteById(courseId);
            if (result == 0) {
                logger.info("Course with courseId=" + courseId + " was not found.");
                return new ResponseEntity<>("Could not find Course with courseId=" + courseId,
                        HttpStatus.NOT_FOUND);

            }
            logger.info("Course with courseId=" + courseId + " was deleted.");
            return new ResponseEntity<>("Course was successfully deleted", HttpStatus.OK);
        } catch (Exception e) {
            logger.warn("Error occurred when deleting a course with courseId=" + courseId
                    +"\nThe error message:\n" + e.getMessage());
            return new ResponseEntity<>("Could not delete the course", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Updates a course in database (using repository).
     * The course is validated by the isCourseValid method before its sent onward to repository.
     *
     * @param courseId ID of the course to update
     * @param course New course values. Course ID is decided by the courseId parameter, therefor
     *               the courseID of this object is ignored. If a field of this object is null,
     *               the corresponding database field will stay unchanged.
     * @return Status message and code, based on if repository could update the course in database
     */
    public ResponseEntity<String> updateCourse(Integer courseId, Course course) {
        try {
            Course c = repository.findById(courseId); // Checking if the course exists
            if (c != null) {
                c.setCourseId(courseId);
                if (course.getCourseName() != null) c.setCourseName(course.getCourseName());
                if (course.getCourseCode() != null) c.setCourseCode(course.getCourseCode());
                if (course.getNumOfAssignments() != null) c.setNumOfAssignments(course.getNumOfAssignments());
                if (course.getNumOfRequiredAssignments() != null)
                    c.setNumOfRequiredAssignments(course.getNumOfRequiredAssignments());
                if (course.getIsQueueOpen() != null) c.setIsQueueOpen(course.getIsQueueOpen());

                // Only updating if data is valid
                if (isCourseValid(c)) {
                    repository.update(c);
                } else {
                    logger.warn("Course attempted updated in database to invalid values:\n" + course.toString());
                    return new ResponseEntity<>("Course is invalid", HttpStatus.BAD_REQUEST);
                }

                logger.info("Updated course with courseId=" + courseId + ". New data: \n" + c.toString());
                return new ResponseEntity<>("Course was updated successfully", HttpStatus.OK);

            } else { // course does not exist
                logger.info("Course with courseId=" + courseId + " was not found.");
                return new ResponseEntity<>("Cannot find course with id=" + courseId, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            logger.warn("Error occurred when updating a course with courseId=" + courseId
                    +"\nThe error message:\n" + e.getMessage());
            return new ResponseEntity<>("Could not update the course", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean isCourseValid(Course course) {
        if (course.getCourseName() == null || course.getCourseName().isBlank()) return false;
        if (course.getNumOfAssignments() == null || course.getNumOfAssignments() < 0) return false;
        if (course.getNumOfRequiredAssignments() == null) return false;
        if (course.getNumOfRequiredAssignments() > course.getNumOfAssignments()) return false;
        return true;
    }
}

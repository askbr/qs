package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is an implementation of UserCourseEnrollmentRepository using JDBC.
 * See UserCourseEnrollmentRepository for javadoc.
 */
@Repository
public class UserCourseEnrollmentRepositoryJdbc implements UserCourseEnrollmentRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Course> findAllByUserId(Integer userId) {
        String query = "SELECT * FROM course " +
                "WHERE course_id " +
                "IN (SELECT course_id FROM course_user_enrollment WHERE user_id=?)";

        return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Course.class), userId);
    }

    @Override
    public int save(Integer userId, Integer courseId) {
        String query = "INSERT INTO course_user_enrollment " +
                "(course_id,user_id)" +
                " VALUES (?,?)";
        return jdbcTemplate.update(query, courseId, userId);
    }

    @Override
    public int delete(Integer userId, Integer courseId) {
        String query = "DELETE FROM course_user_enrollment WHERE course_id=? AND user_id=?";
        return jdbcTemplate.update(query, courseId, userId);
    }
}

package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.UserCourseQueueData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is an implementation of UserCourseQueueRepository using JDBC.
 * See UserCourseQueueRepository for javadoc.
 */
@Repository
public class UserCourseQueueRepositoryJdbc implements UserCourseQueueRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<UserCourseQueueData> findAllCoursesUserIsInQueueFor(Integer userId) {
        String query = "SELECT * FROM course_user_queue WHERE user_id=?";
        return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(UserCourseQueueData.class), userId);
    }

    public List<UserCourseQueueData> findAllUsersInQueueForCourse(Integer courseId) {
        String query = "SELECT * FROM course_user_queue WHERE course_id=?";

        return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(UserCourseQueueData.class), courseId);
    }
    public int save(UserCourseQueueData data) {
        String query = "INSERT INTO course_user_queue(course_id, user_id, enqueue_type, assignment_num," +
                " location ,is_getting_assistance)" +
                " VALUES (?,?,?,?,?, ?)";

        return jdbcTemplate.update(query,
                data.getCourseId(),
                data.getUserId(),
                data.getEnqueueType(),
                data.getAssignmentNum(),
                data.getLocation(),
                data.getIsGettingAssistance());
    }

    public int deleteByIds(Integer userId, Integer courseId) {
        String query = "DELETE FROM course_user_queue WHERE user_id=? AND course_id=? ";
        return jdbcTemplate.update(query, userId, courseId);
    }

    public UserCourseQueueData findByIds(Integer userId, Integer courseId) {
        String query = "SELECT * FROM course_user_queue WHERE user_id=? AND course_id=?";

        try {
            UserCourseQueueData data = jdbcTemplate.queryForObject(query,
                    BeanPropertyRowMapper.newInstance(UserCourseQueueData.class), userId, courseId);
            return data;
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }

    public int update(UserCourseQueueData data) {
        String query = "UPDATE course_user_queue SET enqueue_type=?, assignment_num=?," +
                " location=?, is_getting_assistance=?" +
                " WHERE (user_id=? AND course_id=?)";

        return jdbcTemplate.update(query,
                data.getEnqueueType(),
                data.getAssignmentNum(),
                data.getLocation(),
                data.getIsGettingAssistance(),
                data.getUserId(),
                data.getCourseId());
    }
}

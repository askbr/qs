package no.ntnu.idatt2105.qsbackend.controller;

import no.ntnu.idatt2105.qsbackend.model.User;
import no.ntnu.idatt2105.qsbackend.model.request.EmailRequest;
import no.ntnu.idatt2105.qsbackend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Class defines endpoints for managing a User instance
 */
@CrossOrigin
@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    private UserService service;

    Logger logger = LoggerFactory.getLogger(UserController.class);

    /**
     * Gets metadata for a user (excludes sensitive information) based on email only.
     * (Email is unique for a user in the database)
     *
     * @param req EmailRequest object. (An wrapper for email)
     * @return User metadata
     */
    @PostMapping("/users/metadata")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public User getUserByEmail(@RequestBody EmailRequest req) {
        logger.debug("Endpoint /users/metadata received a POST request.");
        return service.getUserByEmail(req.getEmail());
    }

    /**
     * Gets data for a user (hides password)
     *
     * @param userId User ID
     * @return User data
     */
    @GetMapping("/users/{user_id}")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public User getUser(@PathVariable(value="user_id") Integer userId) {
        logger.debug("Endpoint /users/{user_id} received a GET request.");
        return service.getUser(userId);
    }

    // NEEDS DOCS!
    @PostMapping("/users")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<String> addUser(@RequestBody User user) {
        logger.debug("Endpoint /users received a POST request.");
        return service.addUser(user);
    }

}

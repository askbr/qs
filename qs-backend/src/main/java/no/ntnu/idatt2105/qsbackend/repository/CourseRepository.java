package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Course;

import java.util.List;

/**
 * The CourseRepository interface is implemented by classes that modifies database data
 * related to Courses. The specific operations are specified by the methods of this class.
 * Modification can be done by using JDBC or JPA or other tools that connect Java to Database.
 */
public interface CourseRepository {
    /** @return All courses present in the database */
    List<Course> findAll();

    /**
     * Finds course with specified ID if it exists in the database
     *
     * @param courseId ID of the course to find
     * @return The course (if found)
     */
    Course findById(Integer courseId);

    /**
     * Saves a course in the database
     *
     * @param course The course to save (ID of this object will be ignored and decided by the database)
     * @return The number of rows affected in the database
     */
    int save(Course course);

    /**
     * Delets a course by ID
     *
     * @param courseId ID of the course to delete
     * @return The number of rows affected in the database
     */
    int deleteById(Integer courseId);

    /**
     * Updates a course
     *
     * @param course Course to replace the current course with the same ID
     * @return The number of rows affected in the database
     */
    int update(Course course);

    /**
     * Finds a course from database using course code
     *
     * @param courseCode The course code
     * @return Course of course code
     */
    Course findByCode(String courseCode);
}



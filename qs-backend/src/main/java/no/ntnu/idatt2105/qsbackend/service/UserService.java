package no.ntnu.idatt2105.qsbackend.service;

import no.ntnu.idatt2105.qsbackend.model.User;
import no.ntnu.idatt2105.qsbackend.repository.UserRepositoryJdbc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * This class acts as the layer between endpoints (Controller) and database access (Repository).
 * The class handles validation and other business logic.
 */
@Service
public class UserService {

    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepositoryJdbc repository;

    /**
     * Gets a user based on email from repository. Password and email is excluded when
     * user is returned.
     *
     * @param email Email of the user to get
     * @return User from repository, without email and password fields
     */
    public User getUserByEmail(String email) {
        User user = repository.findByEmail(email);
        if (user != null) user.setPassword(null);

        if (user == null) logger.info("User with email=" + email + " was not found.");
        return user;
    }

    /**
     * Gets a user based on user id from repository. Password is excluded when the
     * user is returned.
     *
     * @param userId ID of the user to get
     * @return User from repository, without password field
     */
    public User getUser(Integer userId) {
        User user = repository.findById(userId);
        user.setPassword(null); // Hiding password
        if (user == null) logger.info("User with userId=" + userId + " was not found.");
        return user;
    }

    /**
     * Adds user to database (using repository).
     * The course is validated by the isCourseValid method before its sent onward to repository.
     * If the email of the new user already exists in the database, the user will not be created.
     *
     * @param user User to add, ID will be ignored and set by database
     * @return Status message and code, based on if repository could add the course to database
     */
    public ResponseEntity<String> addUser(User user) {
        User u = repository.findByEmail(user.getEmail());
        if (u != null) { // email already exists in database
            logger.warn("User attempted stored in database with already existing email:\n" + user.toString());
            return new ResponseEntity<>("There already exists a user with email: " + user.getEmail(),
                    HttpStatus.BAD_REQUEST);
        }
        try {
            // Only saving if the course data is valid
            if (isUserValid(user)) {
                repository.save(user);
            } else {
                logger.warn("User attempted stored in database with invalid values:\n" + user.toString());
                return new ResponseEntity<>("User is invalid", HttpStatus.BAD_REQUEST);
            }

            logger.info("User was stored:\n" + user.toString());
            return new ResponseEntity<>("User was successfully stored", HttpStatus.CREATED);
        } catch (Exception e) {
            logger.warn("Error occurred when adding a new user: \n" + user.toString() +
                    "\n The error message: \n" + e.getMessage());

            return new ResponseEntity<>("Error occurred when trying to add user to database",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean isUserValid(User user) {
        if (user.getFirstName() == null || user.getFirstName().isBlank()) return false;
        if (user.getLastName() == null || user.getLastName().isBlank()) return false;
        if (user.getEmail() == null || user.getEmail().isBlank()) return false;
        if (!user.getRole().equals("USER") && !user.getRole().equals("ADMIN")) return false;
        if (user.getPassword() == null || user.getPassword().isBlank()) return false;
        if (user.getPassword().length() < 6) return false;
        // add email pattern check
        return true;
    }
}

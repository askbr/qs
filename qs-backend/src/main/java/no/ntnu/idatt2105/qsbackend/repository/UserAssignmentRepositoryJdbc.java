package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Assignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is an implementation of UserAssignmentRepository using JDBC.
 * See UserAssignmentRepository for javadoc.
 */
@Repository
public class UserAssignmentRepositoryJdbc implements UserAssignmentRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //WARNING SAME PRIMARY KEY MAY OCCOUR FIX BY ADDING assignmentNum as PRIMARY KEY
    public int save(Integer userId, Integer courseId, Integer assignmentNum) {
        String query = "INSERT INTO user_completed_assignments(course_id,user_id,assignment_num)" +
                " VALUES (?,?,?)";

        return jdbcTemplate.update(query, courseId, userId, assignmentNum);
    }

    @Override
    public List<Assignment> findAllByIds(Integer userId, Integer courseId) {
       String query = "SELECT * FROM user_completed_assignments WHERE user_id=? AND course_id=?";
       return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Assignment.class),
               userId, courseId);
    }
}

package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is an implementation of UserAssistantRepository using JDBC.
 * See UserAssistantRepository for javadoc.
 */
@Repository
public class UserAssistantRepositoryJdbc implements UserAssistantRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Course> findUserAssistantCourses(Integer userId) {
        String query = "SELECT * FROM course WHERE course_id IN (SELECT course_id FROM course_user_assistant WHERE user_id=?)";

        return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Course.class), userId);
    }

    @Override
    public int save(Integer userId, Integer courseId) {
        String query = "INSERT INTO course_user_assistant (course_id,user_id)" +
                " VALUES (?,?)";

        return jdbcTemplate.update(query, courseId, userId);
    }
}

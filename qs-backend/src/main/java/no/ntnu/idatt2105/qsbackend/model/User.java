package no.ntnu.idatt2105.qsbackend.model;

/**
 * The class defines a user with the fields userId, firstName, lastName, email, password and role.
 */
public class User {
    private Integer userId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String role; // Role of the user (either USER or ADMIN)

    /** Empty Constructor. Requirement of Spring Boot.*/
    public User() {}

    /** Constructor with all fields*/
    public User(Integer userId, String firstName, String lastName, String email, String password, String role) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    /** @return ID of this user object*/
    public Integer getUserId() {
        return userId;
    }

    /** Sets the user ID of this user object */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /** @return First name of this user object */
    public String getFirstName() {
        return firstName;
    }

    /** Sets the first name of this user object */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /** @return Last name of this user object */
    public String getLastName() {
        return lastName;
    }

    /** Sets the last name of this user object */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /** @return Email of this user object */
    public String getEmail() {
        return email;
    }

    /** Sets the email of this user object */
    public void setEmail(String email) {
        this.email = email;
    }

    /** @return Password of this user object */
    public String getPassword() {
        return password;
    }

    /** Sets the password of this user object */
    public void setPassword(String password) {
        this.password = password;
    }

    /** @return Role of this user object */
    public String getRole() {
        return role;
    }

    /** Sets the role of this user object */
    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}

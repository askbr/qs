package no.ntnu.idatt2105.qsbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QsBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(QsBackendApplication.class, args);
	}

}

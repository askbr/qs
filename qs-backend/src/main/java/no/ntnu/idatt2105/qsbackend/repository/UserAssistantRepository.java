package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Course;

import java.util.List;

/**
 * The UserAssistantRepository interface is implemented by classes that modifies database data
 * related to User Assistants. The specific operations are specified by the methods of this class.
 * Modification can be done by using JDBC or JPA or other tools that connect Java to Database.
 */
public interface UserAssistantRepository {
    /**
     * Finds all courses a user is assistant in (from the database).
     *
     * @param userId ID of the user
     * @return List of the courses found for the user
     */
    List<Course> findUserAssistantCourses(Integer userId);

    // DOCUMENT!
    int save(Integer userId, Integer courseId);
}

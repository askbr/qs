package no.ntnu.idatt2105.qsbackend.controller;

import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.model.request.CourseIdRequest;
import no.ntnu.idatt2105.qsbackend.service.UserCourseEnrollmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Class defines endpoints for managing the courses a user enrolls in
 */
@CrossOrigin
@RestController
@RequestMapping("/api")
public class UserCourseEnrollmentController {

    @Autowired
    private UserCourseEnrollmentService service;

    Logger logger = LoggerFactory.getLogger(UserCourseEnrollmentController.class);

    /**
     * Gets all courses a user enrolls in
     *
     * @param userId ID of user
     * @return List of courses a user enrolls in
     */
    @GetMapping("/users/{user_id}/courses")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<Course> getAllCoursesOfUser(@PathVariable(value="user_id") Integer userId) {
        logger.debug("Endpoint /users/{user_id}/courses received a GET request.");
        return service.getAllCoursesOfUser(userId);
    }

    /**
     * Adds an existing course to a user
     *
     * @param userId ID of user
     * @param courseIdRequest Object (request body) containing ID of the course
     * @return Response with request status (success/fail)
     */
    @PostMapping("/users/{user_id}/courses")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<String> addCourseToUser(@PathVariable(value="user_id") Integer userId,
                                                  @RequestBody CourseIdRequest courseIdRequest) {
        logger.debug("Endpoint /users/{user_id}/courses received a POST request.");
        return service.addCourseToUser(userId, courseIdRequest);
    }

    /**
     * Deletes a course from a user
     *
     * @param userId ID of user
     * @param courseId ID of course
     * @return Response with request status (success/fail)
     */
    @DeleteMapping("/users/{user_id}/courses/{course_id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<String> deleteCourseFromUser(@PathVariable(value="user_id") Integer userId,
                                                       @PathVariable(value="course_id") Integer courseId) {
        logger.debug("Endpoint /users/{user_id}/courses/{course_id} received a DELETE request.");
        return service.deleteCourseFromUser(userId, courseId);
    }
}

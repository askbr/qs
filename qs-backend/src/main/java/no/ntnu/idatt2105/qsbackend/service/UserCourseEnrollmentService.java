package no.ntnu.idatt2105.qsbackend.service;

import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.model.request.CourseIdRequest;
import no.ntnu.idatt2105.qsbackend.repository.UserCourseEnrollmentRepositoryJdbc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class acts as the layer between endpoints (Controller) and database access (Repository).
 * The class handles validation and other business logic.
 */
@Service
public class UserCourseEnrollmentService {

    Logger logger = LoggerFactory.getLogger(UserCourseEnrollmentService.class);

    @Autowired
    private UserCourseEnrollmentRepositoryJdbc repository;

    /**
     * Gets all courses a user enrolls in from database (using repository)
     *
     * @param userId ID of the user
     * @return List of courses the user enrolls in
     */
    public List<Course> getAllCoursesOfUser(Integer userId) {
        return repository.findAllByUserId(userId);
    }

    /**
     * Adds a course to a user (meaning the user enrolls in the course). This is done using repository.
     *
     * @param userId ID of the user
     * @param courseIdRequest Object containing ID of the course the user will enroll (be a part of as a student)
     * @return Status message and code, based on if repository could add the user to the course in the database
     */
    public ResponseEntity<String> addCourseToUser(Integer userId, CourseIdRequest courseIdRequest) {
        try {
            repository.save(userId, courseIdRequest.getCourseId());

            logger.info("Course with courseId=" + courseIdRequest.getCourseId()
                    + " was added to user with userId=" + userId);
            return new ResponseEntity<>("Course successfully added to user.", HttpStatus.CREATED);
        } catch (Exception e) {
            logger.warn("Error occurred when adding course with courseId=" + courseIdRequest.getCourseId()
                    + " to user with userId=" + userId
                    + "\n The error message: \n" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Deletes a course enrollment for a user
     *
     * @param userId ID of the user
     * @param courseId  ID of the course to delete from user
     * @return Status message and code, based on if repository could delete the course enrollment
     * for the user in the database
     */
    public ResponseEntity<String> deleteCourseFromUser(Integer userId, Integer courseId) {
        try {
            int result = repository.delete(userId, courseId);
            if (result == 0) {
                logger.info("Could not delete course with courseId=" + courseId + " from user with userId=" + userId);
                return new ResponseEntity<>("User with id="+ userId +
                        " does not exist or the user does not have a course with id=" + courseId, HttpStatus.NOT_FOUND);
            }
            logger.info("Deleted course with courseId=" + courseId + " from user with userId=" + userId);
            return new ResponseEntity<>("Course successfully deleted from user", HttpStatus.OK);
        } catch (Exception e) {
            logger.warn("Error occurred when deleting a course with courseId=" + courseId
                    +"from user with userId=" + userId + "\nThe error message:\n" + e.getMessage());

            return new ResponseEntity<>("Cannot delete course from user", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

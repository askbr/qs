package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Course;

import java.util.List;

/**
 * The UserCourseEnrollmentRepository interface is implemented by classes that modifies database data
 * related to User Enrollment in Courses. The specific operations are specified by the methods of this
 * class. Modification can be done by using JDBC or JPA or other tools that connect Java to Database.
 */
interface UserCourseEnrollmentRepository {
    /**
     * Finds all courses a user enrolls in (from the database).
     *
     * @param userId ID of the user
     * @return List of courses found
     */
    List<Course> findAllByUserId(Integer userId);

    /**
     * Saves a user enrollment in the database
     *
     * @param userId ID of the user
     * @param courseId ID of the course
     * @return The number of rows affected in the database
     */
    int save(Integer userId, Integer courseId);

    /**
     * Deletes a course enrollment from the database
     *
     * @param userId ID of the user
     * @param courseId ID of the course
     * @return The number of rows affected in the database
     */
    int delete(Integer userId, Integer courseId);
}

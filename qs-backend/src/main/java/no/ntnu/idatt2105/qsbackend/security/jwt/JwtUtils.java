package no.ntnu.idatt2105.qsbackend.security.jwt;

import com.google.common.base.Strings;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import no.ntnu.idatt2105.qsbackend.model.User;
import no.ntnu.idatt2105.qsbackend.repository.UserRepositoryJdbc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JwtUtils {

    @Autowired
    JwtConfig jwtConfig;

    @Autowired
    UserRepositoryJdbc userRepositoryJdbc;

    public User getUserOfToken(String encryptedToken) {
        if (Strings.isNullOrEmpty(encryptedToken) ||
                !encryptedToken.startsWith(jwtConfig.getTokenPrefix())) {
            return null;
        }

        // Grabbing token, removing the "Bearer " prefix
        String token = encryptedToken.replace(jwtConfig.getTokenPrefix(), "");
        try {
            // Decrypting the jwt
            Jws<Claims> claimsJws = Jwts.parserBuilder()
                    .setSigningKey(jwtConfig.getSecretKey().getBytes())
                    .build()
                    .parseClaimsJws(token);

            String email = claimsJws.getBody().getSubject();
            User user = userRepositoryJdbc.findByEmail(email);

            if (user == null) return null;
            return user;

        } catch (JwtException e) {
            return null;
        }
    }
}

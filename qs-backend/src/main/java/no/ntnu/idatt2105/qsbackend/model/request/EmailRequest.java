package no.ntnu.idatt2105.qsbackend.model.request;

/**
 * The class represents a request-body when sending an email to this Spring Boot Application
 */
public class EmailRequest {
    private String email;

    /** @return email of this object */
    public String getEmail() {
        return email;
    }

    /** Sets email of this object */
    public void setEmail(String email) {
        this.email = email;
    }
}

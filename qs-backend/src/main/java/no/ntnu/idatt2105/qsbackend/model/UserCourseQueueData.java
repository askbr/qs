package no.ntnu.idatt2105.qsbackend.model;

/**
 * The class defines data related to a user in queue for a course. The fields courseId, userId,
 * enqueueType, assignmentNum and isGettingHelp are included to define the queue status for a
 * user in a specific course.
 */

public class UserCourseQueueData {
    private Integer courseId; // ID of the course the user is enqueued for
    private Integer userId; // ID of the user in queue for the course
    private Boolean enqueueType; // Type of queue entry. True: approve an assignment, False: get help with assignment.
    private Integer assignmentNum; // The number of the assignment the user is in queue for.
    private String location; // Location of the user to be helped
    private Boolean isGettingAssistance; // True: the user is currently getting attended by assistant. False: not attended.

    /** Empty Constructor. Requirement of Spring Boot.*/
    public UserCourseQueueData() {}

    /** Constructor with all fields */
    public UserCourseQueueData(Integer courseId, Integer userId, Boolean enqueueType, Integer assignmentNum,
                               String location, Boolean isGettingAssistance) {
        this.courseId = courseId;
        this.userId = userId;
        this.enqueueType = enqueueType;
        this.assignmentNum = assignmentNum;
        this.location = location;
        this.isGettingAssistance = isGettingAssistance;
    }

    /** @return Location of user in queue */
    public String getLocation() {
        return location;
    }

    /** Sets location of the user */
    public void setLocation(String location) {
        this.location = location;
    }

    /** @return ID of the course the user with ID userId is in queue for */
    public Integer getCourseId() {
        return courseId;
    }

    /** Sets the course ID of this object */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /** @return ID of the user in queue for the course with ID courseId */
    public Integer getUserId() {
        return userId;
    }

    /** Sets the user ID of this object */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /** @return Type of queue entry. True: approve an assignment, False: get help with assignment. */
    public Boolean getEnqueueType() {
        return enqueueType;
    }

    /** Sets the enqueue type of this object */
    public void setEnqueueType(Boolean enqueueType) {
        this.enqueueType = enqueueType;
    }

    /** @return The number of the assignment the user is in queue for */
    public Integer getAssignmentNum() {
        return assignmentNum;
    }

    /** Sets the assignment number of this object */
    public void setAssignmentNum(Integer assignmentNum) {
        this.assignmentNum = assignmentNum;
    }

    /** @return True: the user is currently getting attended by assistant. False: not attended. */
    public Boolean getIsGettingAssistance() {
        return isGettingAssistance;
    }

    /** Sets the isGettingAssistance status of this object */
    public void setIsGettingAssistance(Boolean isGettingAssistance) {
        this.isGettingAssistance = isGettingAssistance;
    }

    @Override
    public String toString() {
        return "UserCourseQueueData{" +
                "courseId=" + courseId +
                ", userId=" + userId +
                ", enqueueType=" + enqueueType +
                ", assignmentNum=" + assignmentNum +
                ", location='" + location + '\'' +
                ", isGettingAssistance=" + isGettingAssistance +
                '}';
    }
}

package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Assignment;

import java.util.List;

/**
 * The UserAssignmentRepository interface is implemented by classes that modifies database data
 * related to User Assignments. The specific operations are specified by the methods of this class.
 * Modification can be done by using JDBC or JPA or other tools that connect Java to Database.
 */
public interface UserAssignmentRepository {
    /**
     * Saves a user assignment completion in the database.
     *
     * @param userId ID of the user
     * @param courseId ID of the course
     * @param assignmentNum The assignment number
     * @return The number of rows affected in the database
     */
    int save(Integer userId, Integer courseId, Integer assignmentNum);

    /**
     * Finds all approved assignments for user in course (from database)
     * @param userId ID of the user
     * @param courseId ID of the course
     * @return List of found assignments
     */
    List<Assignment> findAllByIds(Integer userId, Integer courseId);
}

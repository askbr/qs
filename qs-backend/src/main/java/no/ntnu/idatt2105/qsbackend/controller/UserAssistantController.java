package no.ntnu.idatt2105.qsbackend.controller;

import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.model.request.CourseIdRequest;
import no.ntnu.idatt2105.qsbackend.service.UserAssistantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Class defines endpoints for managing user assistants for different courses
 */
@RestController
@RequestMapping("/api")
public class UserAssistantController {
    @Autowired
    private UserAssistantService service;

    Logger logger = LoggerFactory.getLogger(UserAssistantController.class);

    /**
     * Gets courses a user is assisting
     *
     * @param userId ID of user
     *
     * @return List of courses where the given user is an assistant
     */
    @GetMapping("/users/{user_id}/assistant/courses")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<Course> getUserAssistantCourses(@PathVariable(value="user_id") Integer userId) {
        logger.debug("Endpoint /users/{user_id}/assistant/courses received a GET request.");
        return service.getUserAssistantCourses(userId);
    }

    // DOCUMENT!

    /**
     * Adds a user assistant to course
     *
     * @param userId ID of the user assistant to be added
     * @param courseIdRequest Object containing the id of the course the user is to be assistant in
     * @return Response with request status (success/fail)
     */
    @PostMapping("/users/{user_id}/assistant/courses")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<String> addUserAssistantToCourse(@PathVariable(value="user_id") Integer userId,
                                                           @RequestBody CourseIdRequest courseIdRequest) {
        logger.debug("Endpoint /users/{user_id}/assistant/courses received a POST request.");
        return service.addUserAssistantToCourse(userId, courseIdRequest);

    }
}

package no.ntnu.idatt2105.qsbackend.model.request;

public class CourseCodeRequest {
    private String courseCode;

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }
}

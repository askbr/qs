package no.ntnu.idatt2105.qsbackend.service;

import no.ntnu.idatt2105.qsbackend.repository.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * This class acts as the layer between endpoints (Controller) and database access (Repository).
 * The class handles validation and other business logic.
 */
@Service
public class ApplicationUserService implements UserDetailsService {

    @Autowired
    private ApplicationUserRepository applicationUserDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return applicationUserDao
                .selectApplicationUserByUsername(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException(String.format("Username %s not found", username))
                );
    }
}

package no.ntnu.idatt2105.qsbackend.controller;

import no.ntnu.idatt2105.qsbackend.model.UserCourseQueueData;
import no.ntnu.idatt2105.qsbackend.service.UserCourseQueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Class defines endpoints for managing the queue of a user. A user-queue contains courses
 * the user is waiting for an assistant to help/approve an assignment
 */
@CrossOrigin
@RestController
@RequestMapping("/api")
public class UserCourseQueueController {
    @Autowired
    private UserCourseQueueService service;

    Logger logger = LoggerFactory.getLogger(UserCourseQueueController.class);

    /**
     * Gets all courses a user with given userId is in queue for.
     * Information about the queue status for the user is also included.
     *
     * @param userId ID of user
     * @return List of queue data for all courses an give user is enqueued
     */
    @GetMapping("/users/{user_id}/queue/courses")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<UserCourseQueueData> getCoursesUserIsInQueueFor(@PathVariable(value="user_id") Integer userId) {
        logger.debug("Endpoint /users/{user_id}/queue/courses received a GET request.");
        return service.getCoursesUserIsInQueueFor(userId);
    }

    /**
     * Gets all users in queue for a given course.
     * Information about the queue status for the user is also included.
     *
     * @param courseId ID of course
     * @return List of queue data for all users enqueued in a given course
     */
    @GetMapping("/courses/{course_id}/queue/users")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<UserCourseQueueData> getUsersInQueueForCourse(@PathVariable(value="course_id") Integer courseId) {
        logger.debug("Endpoint /courses/{course_id}/queue/users received a GET request.");
        return service.getUsersInQueueForCourse(courseId);
    }

    /**
     * Adds user to queue in a given course
     *
     * @param  courseId ID of the course
     * @param userCourseData Data for the new queue entry, including courseId and userId
     * @param token Token used to make the request
     * @return Response with request status (success/fail)
     */
    @PostMapping("/courses/{course_id}/queue/users")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<String> addUserToQueueInCourse(@PathVariable(value="course_id") Integer courseId,
                                                         @RequestBody UserCourseQueueData userCourseData,
                                                         @RequestHeader(name="Authorization") String token) {
        logger.debug("Endpoint /courses/{course_id}/queue/users received a POST request.");
        return service.addUserToQueueInCourse(courseId, userCourseData, token);
    }

    /**
     * Gets queue info for user in a given course
     *
     * @param courseId ID of course
     * @param userId ID of user
     * @return Queue info
     */
    @GetMapping("/courses/{course_id}/queue/users/{user_id}")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public UserCourseQueueData getQueueInfoForUserInCourse(@PathVariable(value="course_id")Integer courseId,
                                                           @PathVariable(value="user_id") Integer userId) {
        logger.debug("Endpoint /courses/{course_id}/queue/users/{user_id} received a GET request.");
        return service.getQueueInfoForUserInCourse(userId, courseId);
    }

    /**
     * Removes user from the queue of a course
     *
     * @param courseId ID of course to be dequeued in
     * @param userId ID of user to dequeue
     * @return Response with request status (success/fail)
     */
    @DeleteMapping("/courses/{course_id}/queue/users/{user_id}")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<String> removeUserFromQueueInCourse(@PathVariable(value="course_id")Integer courseId,
                                                              @PathVariable(value="user_id") Integer userId) {
        logger.debug("Endpoint /courses/{course_id}/queue/users/{user_id} received a DELETE request.");
        return service.removeUserFromQueueInCourse(userId, courseId);
    }

    /**
     * Updates the user queue data in a given course
     *
     * @param courseId ID of course
     * @param userId ID of user
     * @param req Updated user-queue data
     * @return Response with request status (success/fail)
     */
    @PutMapping("/courses/{course_id}/queue/users/{user_id}")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<String> updateQueueStateForUserInCourse(@PathVariable(value="course_id")Integer courseId,
                                                                  @PathVariable(value="user_id") Integer userId,
                                                                  @RequestBody UserCourseQueueData req) {
        logger.debug("Endpoint /courses/{course_id}/queue/users/{user_id} received a PUT request.");
        return service.updateQueueStateForUserInCourse(userId, courseId, req);
    }
}

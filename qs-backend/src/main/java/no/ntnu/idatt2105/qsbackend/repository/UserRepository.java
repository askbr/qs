package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.model.User;

/**
 * The UserRepository interface is implemented by classes that modifies database data
 * related to Users. The specific operations are specified by the methods of this class.
 * Modification can be done by using JDBC or JPA or other tools that connect Java to Database.
 */
public interface UserRepository {

    /**
     * Finds user with specified email if it exists in the database
     *
     * @param email Email of the user
     * @return The user (if found)
     */
    User findByEmail(String email);

    /**
     * Finds user with specified user ID if it exists in the database
     *
     * @param userId ID of the user
     * @return The user (if found)
     */
    User findById(Integer userId);

    /**
     * Saves a user in the database
     *
     * @param user The user to save (ID of this object will be ignored and decided by the database)
     * @return The number of rows affected in the database
     */
    int save(User user);
}

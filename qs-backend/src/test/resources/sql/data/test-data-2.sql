INSERT INTO user (user_id, first_name, last_name, email, password, role) VALUES
    (11, "Super", "Man", "super@man.com", "sm", "USER"),
    (12, "Spider", "Man", "spider@man.com", "sm", "ADMIN"),
    (13, "Iron", "Man", "iron@man.com", "im", "USER");                                                                             ;

INSERT INTO course (course_id, course_name, course_code, num_of_assignments, num_of_required_assignments, is_queue_open) VALUES
    (1, "Space", "NASA", 10, 8, 0),
    (2, "Algo", "BRA", 6, 6, 1),
    (3, "X", "Y", 100, 100, 0);

INSERT INTO course_user_queue (course_id, user_id, enqueue_type, assignment_num, location, is_getting_assistance) VALUES
(1, 11, 1, 5, "mars", 0), (2, 12, 0, 1, "earth", 0), (3, 13, 0, 97, "moon", 1);
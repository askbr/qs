CREATE TABLE user (
    user_id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(32),
    last_name VARCHAR(32),
    email VARCHAR(64) NOT NULL,
    password VARCHAR(512) NOT NULL,
    role VARCHAR(16) NOT NULL,
    PRIMARY KEY (user_id)
);

CREATE TABLE course (
    course_id INT NOT NULL AUTO_INCREMENT,
    course_name VARCHAR(64) NOT NULL,
    course_code VARCHAR(16),
    num_of_assignments INT NOT NULL,
    num_of_required_assignments INT,
    is_queue_open BIT NOT NULL,
    PRIMARY KEY (course_id)
);

CREATE TABLE course_user_assistant (
    course_id INT NOT NULL,
    user_id INT NOT NULL,
    FOREIGN KEY (course_id) REFERENCES course (course_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (user_id) ON DELETE CASCADE,
    PRIMARY KEY (course_id, user_id)
);

CREATE TABLE course_user_enrollment (
    course_id INT NOT NULL,
    user_id INT NOT NULL,
    FOREIGN KEY (course_id) REFERENCES course (course_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (user_id) ON DELETE CASCADE,
    PRIMARY KEY (course_id, user_id)
);

CREATE TABLE course_user_queue (
    course_id INT NOT NULL,
    user_id INT NOT NULL,
    enqueue_type BIT NOT NULL,
    assignment_num INT NOT NULL,
    location VARCHAR(64),
    is_getting_assistance BIT NOT NULL,
    FOREIGN KEY (course_id) REFERENCES course (course_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (user_id) ON DELETE CASCADE,
    PRIMARY KEY (course_id, user_id)
);

CREATE TABLE user_completed_assignments (
    course_id INT NOT NULL,
    user_id INT NOT NULL,
    assignment_num INT NOT NULL,
    FOREIGN KEY (course_id) REFERENCES course (course_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (user_id) ON DELETE CASCADE,
    PRIMARY KEY (course_id, user_id, assignment_num)
);
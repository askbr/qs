package no.ntnu.idatt2105.qsbackend.service;

import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.model.User;
import no.ntnu.idatt2105.qsbackend.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@Nested
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Test
    @DisplayName("the method addUser returns a response entity with bad request status on invalid input")
    @Sql("/sql/setup/clean-tables.sql")
    public void addUserReturnsBadRequestOnInvalidInput() {

        ResponseEntity<String> response2 = userService.addUser(
                new User(2,"   ", "te22", "ttd@mail.com", "1234567", "ADMIN") // invalid: firstname is blank
        );
        ResponseEntity<String> response3 = userService.addUser(
                new User(3,"te32", null, "ttt@mail.com", "1234567", "ADMIN") // invalid: lastname is null
        );
        ResponseEntity<String> response4 = userService.addUser(
                new User(4,"tet1", "tet2", "tttt@mail.com", "1234567", "aaa") // invalid: role is not ADMIN or STUDENT
        );
        ResponseEntity<String> response5 = userService.addUser(
                new User(5,"te22", "te22", "tyyyy@mail.com", "123", "ADMIN") // invalid: password length  < 6
        );

        assertEquals(response2.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(response3.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(response4.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(response5.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("the addCourse method returns status created on correct input")
    @Sql("/sql/setup/clean-tables.sql")
    public void addUserReturnsStatusCreatedOnCorrectInput() {
        ResponseEntity<String> response1 = userService.addUser(
                new User(11,"studenta", "studenta", "ttt@mail.com", "1234567", "USER")
        );
        ResponseEntity<String> response2 = userService.addUser(
                new User(22,"studentb", "studentb", "ddd@mail.com", "1234567", "USER")
        );
        ResponseEntity<String> response3 = userService.addUser(
                new User(33,"admin", "admin", "aaa@mail.com", "1234567", "ADMIN")
        );

        assertEquals(response1.getStatusCode(), HttpStatus.CREATED);
        assertEquals(response2.getStatusCode(), HttpStatus.CREATED);
        assertEquals(response3.getStatusCode(), HttpStatus.CREATED);
    }

}

package no.ntnu.idatt2105.qsbackend.repository;

import no.ntnu.idatt2105.qsbackend.model.UserCourseQueueData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

/**
 * Class for testing UserCourseQueueRepositoryJdbc.
 * Before starting the tests of this class, be sure to have configured application-test.properties
 * properly and have a clean database with the tables defined in resources/setup/setup-db.sql
 */
@SpringBootTest
@ActiveProfiles("test")
@Nested
public class UserCourseQueueRepositoryJdbcTest {

    @Autowired
    UserCourseQueueRepositoryJdbc repository;

    @Test
    @DisplayName("findAllCoursesUserIsInQueueFor method finds all the courses a user is in queue for")
    @Sql({"/sql/setup/clean-tables.sql", "/sql/data/test-data-1.sql"})
    public void findAllCoursesUserIsInQueueForFindsAllCourses() {
        List<UserCourseQueueData> d1 = repository.findAllCoursesUserIsInQueueFor(11);
        List<UserCourseQueueData> d2 = repository.findAllCoursesUserIsInQueueFor(12);
        List<UserCourseQueueData> d3 = repository.findAllCoursesUserIsInQueueFor(13);

        assertEquals(d1.size(), 2);
        assertEquals(d2.size(), 1);
        assertEquals(d3.size(), 3);
    }

    @Test
    @DisplayName("findAllCoursesUserIsInQueueFor method finds all the courses a user is in queue for")
    @Sql({"/sql/setup/clean-tables.sql", "/sql/data/test-data-2.sql"})
    public void findAllCoursesUserIsInQueueForFindsTheCorrectCourses() {
        List<UserCourseQueueData> d1 = repository.findAllCoursesUserIsInQueueFor(11);
        List<UserCourseQueueData> d2 = repository.findAllCoursesUserIsInQueueFor(12);
        List<UserCourseQueueData> d3 = repository.findAllCoursesUserIsInQueueFor(13);

        assertEquals(d1.get(0).getCourseId(), 1);
        assertEquals(d1.get(0).getUserId(), 11);
        assertEquals(d1.get(0).getEnqueueType(), true);
        assertEquals(d1.get(0).getAssignmentNum(), 5);
        assertEquals(d1.get(0).getLocation(), "mars");
        assertEquals(d1.get(0).getIsGettingAssistance(), false);

        assertEquals(d2.get(0).getCourseId(), 2);
        assertEquals(d2.get(0).getUserId(), 12);
        assertEquals(d2.get(0).getEnqueueType(), false);
        assertEquals(d2.get(0).getAssignmentNum(), 1);
        assertEquals(d2.get(0).getLocation(), "earth");
        assertEquals(d2.get(0).getIsGettingAssistance(), false);

        assertEquals(d3.get(0).getCourseId(), 3);
        assertEquals(d3.get(0).getUserId(), 13);
        assertEquals(d3.get(0).getEnqueueType(), false);
        assertEquals(d3.get(0).getAssignmentNum(), 97);
        assertEquals(d3.get(0).getLocation(), "moon");
        assertEquals(d3.get(0).getIsGettingAssistance(), true);

    }

    @Test
    @DisplayName("findAllCoursesUserIsInQueueFor method finds all the courses a user is in queue for")
    @Sql({"/sql/setup/clean-tables.sql", "/sql/data/test-data-2.sql"})
    public void findAllUsersInQueueForCourseFindsTheCorrectUsers() {
        List<UserCourseQueueData> d1 = repository.findAllUsersInQueueForCourse(1);
        List<UserCourseQueueData> d2 = repository.findAllUsersInQueueForCourse(2);
        List<UserCourseQueueData> d3 = repository.findAllUsersInQueueForCourse(3);

        assertEquals(d1.get(0).getCourseId(), 1);
        assertEquals(d1.get(0).getUserId(), 11);
        assertEquals(d1.get(0).getEnqueueType(), true);
        assertEquals(d1.get(0).getAssignmentNum(), 5);
        assertEquals(d1.get(0).getLocation(), "mars");
        assertEquals(d1.get(0).getIsGettingAssistance(), false);

        assertEquals(d2.get(0).getCourseId(), 2);
        assertEquals(d2.get(0).getUserId(), 12);
        assertEquals(d2.get(0).getEnqueueType(), false);
        assertEquals(d2.get(0).getAssignmentNum(), 1);
        assertEquals(d2.get(0).getLocation(), "earth");
        assertEquals(d2.get(0).getIsGettingAssistance(), false);

        assertEquals(d3.get(0).getCourseId(), 3);
        assertEquals(d3.get(0).getUserId(), 13);
        assertEquals(d3.get(0).getEnqueueType(), false);
        assertEquals(d3.get(0).getAssignmentNum(), 97);
        assertEquals(d3.get(0).getLocation(), "moon");
        assertEquals(d3.get(0).getIsGettingAssistance(), true);
    }

}

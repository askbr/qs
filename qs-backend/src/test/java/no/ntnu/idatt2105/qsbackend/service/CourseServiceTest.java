package no.ntnu.idatt2105.qsbackend.service;

import no.ntnu.idatt2105.qsbackend.model.Course;
import no.ntnu.idatt2105.qsbackend.repository.CourseRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@Nested
public class CourseServiceTest {

    @Autowired
    CourseService courseService;

    @Test
    @DisplayName("the method addCourse returns a response entity with bad request status on invalid input")
    @Sql("/sql/setup/clean-tables.sql")
    public void addCourseReturnsBadRequestOnInvalidInput() {
        ResponseEntity<String> response1 = courseService.addCourse(
                new Course(10, "name", "code", 5, 6, false) // invalid: numOfRequiredAssignments > numOfAssignments
        );
        ResponseEntity<String> response2 = courseService.addCourse(
                new Course(11, "              ", "code", 10, 1, false) // invalid: name is blank
        );
        ResponseEntity<String> response3 = courseService.addCourse(
                new Course(10, null, "code", 10, 1, false) // invalid: name is null
        );
        ResponseEntity<String> response4 = courseService.addCourse(
                new Course(10, "name", "code", -1, -5, false) // invalid: numOfAssignments < 0
        );
        ResponseEntity<String> response5 = courseService.addCourse(
                new Course(10, "name", "code", 10, null, false) // invalid: numOfRequiredAssignments is null
        );


        assertEquals(response1.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(response2.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(response3.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(response4.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(response5.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("the addCourse method returns status created on correct input")
    @Sql("/sql/setup/clean-tables.sql")
    public void addCourseReturnsStatusCreatedOnCorrectInput() {
        ResponseEntity<String> response1 = courseService.addCourse(
                new Course(null, "Physics", "PHYS", 10, 5, null)
        );
        ResponseEntity<String> response2 = courseService.addCourse(
                new Course(null, "X", "X", 300, 2, null)
        );
        ResponseEntity<String> response3 = courseService.addCourse(
                new Course(10, "Physics", "PHYSS", 100, 100, true)
        );

        assertEquals(response1.getStatusCode(), HttpStatus.CREATED);
        assertEquals(response2.getStatusCode(), HttpStatus.CREATED);
        assertEquals(response3.getStatusCode(), HttpStatus.CREATED);
    }
}

package no.ntnu.idatt2105.qsbackend.service;


import no.ntnu.idatt2105.qsbackend.model.UserCourseQueueData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@Nested
public class UserCourseQueueServiceTest {

    @Autowired
    UserCourseQueueService userCourseQueueService;

    // if this test failed, so this token is expired.
    String tokenForTest = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzQHUuZ2ciLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoiUk9MRV9BRE1JTiJ9XSwiaWF0IjoxNjQ5MTAwNjgyLCJleHAiOjE2NDk2MjgwMDB9.49uFrLCnRkjjqZaple9u1oqRM87gaif1nce_2YTcozCLazqK1rt3cgmYYYqFnkBL6guL51eVSI9Rof1Y3wHAVw";

    @Test
    @DisplayName("the method addUserToQueueInCourse returns a response entity with INTERNAL_SERVER_ERROR status when a user is not in this course")
    @Sql({"/sql/setup/clean-tables.sql", "/sql/data/test-data-3.sql"})
    public void addUserReturnsBadRequestOnInvalidInput() {

        ResponseEntity<String> response1 = userCourseQueueService.addUserToQueueInCourse(2,new UserCourseQueueData( 2,  20,  true,  2,
                 "table1",  false  ), tokenForTest); // this student is not in course 2

        assertEquals(response1.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

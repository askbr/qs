import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'
import axios from "axios";

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    state: {
      isAuthenticated: false,
      token: null,
      role: 'USER',
      userId: null,
      firstName: '',
      lastName: '',
    },

    mutations: {
      SET_IS_AUTHENTICATED(state, isAuthenticated) {
        state.isAuthenticated = isAuthenticated;
      },
      SET_TOKEN(state, token) {
        state.token = token;
      },
      SET_ROLE(state, role) {
        state.role = role;
      },
      SET_USERID(state, userId) {
        state.userId = userId;
      },
      SET_FIRST_NAME(state, firstName) {
        state.firstName = firstName;
      },
      SET_LAST_NAME(state, lastName) {
        state.lastName = lastName;
      },

    },
    actions: {
      saveToken({ commit }, token) {
        commit('SET_TOKEN',token)
      },
      saveRole({ commit }, role) {
        commit('SET_ROLE', role);
      },
      saveUserId({ commit }, userId) {
        commit('SET_USERID',userId)
      },
      authenticate({ commit }) {
        commit('SET_IS_AUTHENTICATED', true);
      },
      authenticateFalse({ commit }) {
        commit('SET_IS_AUTHENTICATED', false);
      },
      setFirstName({ commit }, firstName) {
        commit('SET_FIRST_NAME', firstName);
      },
      setLastName({ commit }, lastName) {
        commit('SET_LAST_NAME', lastName);
      }

    }, getters: {
      getToken (state) {
        return state.token
      },
      getUserId (state) {
        return state.userId
      },
      getIsAuthenticated(state) {
        return state.isAuthenticated
      },
      getRole(state) {
        return state.role;
      },
      getFirstName(state) {
        return state.firstName;
      },
      getLastName(state) {
        return state.lastName;
      },
      getPosition(state) {
        return state.position;
      },
      getApiClient(state) {
        return axios.create({
          baseURL: 'http://localhost:8090/api',
          headers: {
            'Authorization': state.token
          }
        })
      }

    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  return Store
})

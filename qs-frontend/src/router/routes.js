import Login from "pages/Login.vue";
import CourseList from "components/CourseList";
import EditCourse from "pages/EditCourse";
import AdminHome from "pages/AdminHome";
import AddNewCourse from "pages/AddNewCourse";
import UserHome from "pages/UserHome";
import CourseQueue from "pages/CourseQueue";
import ActiveQueue from "pages/ActiveQueue";
import WaitInQueue from "pages/WaitInQueue";
import PickedStudent from "pages/PickedStudent";
import AssistantPage from "pages/AssistantPage";
import CoursePage from "pages/CoursePage";


const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        redirect: { name: "Login"}
      },
      {
        path: "/home/user",
        name: "UserHome",
        component: UserHome,
      },
      {
        path: "/home/admin",
        name: "AdminHome",
        component: AdminHome,
      },
      {
        path: "/login",
        name: "Login",
        component: Login,
      },
      {
        path: "/courseList",
        name: "CourseList",
        component: CourseList,
      },
      {
        path: "/courses/new",
        name: "AddNewCourse",
        component: AddNewCourse,
      },
      {
        path: "/courses/:id/edit",
        name: "EditCourse",
        props: true, // it takes the course id as prop when we press on it in courses list (dynamic routing)
        component: EditCourse,
      },
      {
        path: "/courses/:id/enqueue",
        name: "CourseQueue",
        props: true, // it takes the course id as prop when we press on it in courses list (dynamic routing)
        component: CoursePage,
      },
      {
        path: "/courses/:id/queue",
        name: "ActiveQueue",
        props: true,
        component: ActiveQueue,
      },
      {
        path: "/courses/wait",
        name: "WaitInQueue",
        props: true,
        component: WaitInQueue,
      },
      {
        path: "/courses/:cid/help/:uid",
        name: "PickedStudent",
        props: true,
        component: PickedStudent,
      },
      {
        path: "/assistant",
        name: "AssistantPage",
        component: AssistantPage,
      },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  },

]

export default routes

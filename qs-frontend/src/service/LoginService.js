import axios from "axios";
// true: login success, false: login failed
let attemptLogin = function(loginRequest) {
  return axios.post("http://localhost:8090/login", loginRequest);

}

export default { attemptLogin };

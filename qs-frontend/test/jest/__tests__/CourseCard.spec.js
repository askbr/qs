import { describe, expect, it } from '@jest/globals';
import { mount } from '@vue/test-utils';
import MyButton from './demo/MyButton';
import CourseCard from "components/CourseCard";

/**
 * This component takes a course as a prop
 */

const courseTestData = {
  courseId: 232,
    courseCode: "DATA",
    numOfAssignments: 15,
    numOfRequiredAssignments: 10,
    isQueueOpen: true
};

describe('MyButton', () => {
  it('has toCourse method', () => {
    const wrapper = mount(CourseCard, {
      props: {
        course: courseTestData
      }
    });
    const { vm } = wrapper;
    expect(typeof vm.toCourse).toBe('function');
  });

  it('queue status is correct based on course being displayed in the card',
    async () => {
      const wrapper = mount(CourseCard, {
        props: {
          course: courseTestData
        }
      });
      let expectedHtmlText = "Queue is Open";

    expect(wrapper.html().includes(expectedHtmlText)).toEqual(true);
  });
});

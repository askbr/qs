import {beforeEach, describe, expect, it, jest} from '@jest/globals';
import { mount } from '@vue/test-utils';
import axios from "axios";
import MainLayout from "layouts/MainLayout";


jest.mock('axios')

const mockRoute = {
  params: {
    id: 1,
    cid: 2
  }
}

const mockRouter = {
  push: jest.fn()
}

const $store = {
  state: {
    role: "STUDENT"
  },
  commit: jest.fn(),
  getters: jest.fn(),
  getIsAuthenticated : jest.fn(()=> true)
}
let wrapper

describe('MainLayout', () => {
  // render the component
  beforeEach(() => {
    wrapper = mount(MainLayout,{
        global: {
          mocks: {
            $route: mockRoute,
            $router: mockRouter,
            $store
          }
        }
      }
    );
  })

  it('check that MainLayout renders', () => {

    expect(wrapper.exists()).toBeTruthy();
  })


})

import {beforeEach, describe, expect, it, jest} from '@jest/globals';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-jest';
import { mount } from '@vue/test-utils';
import Login from "pages/Login";
import LoginService from "src/service/LoginService.js"
import axios from "axios";

jest.mock('axios')

const mockRoute = {
  params: {
    id: 1,
    cid: 2
  }
}

const mockRouter = {
  push: jest.fn()
}

const $store = {
  state: {
    isAuthenticated: true
  },
  commit: jest.fn(),
  getters: jest.fn(),
}
let wrapper

describe('Login', () => {

  // render the component
  beforeEach(() => {
    wrapper = mount(Login,{
        global: {
          mocks: {
            $route: mockRoute,
            $router: mockRouter,
            $store
          }
        }
      }
      );
  })

  it('check that Login renders', () => {

    expect(wrapper.exists()).toBeTruthy();
  })

  it('has routeBasedOnRole method', () => {

    const { vm } = wrapper;

    expect(typeof vm.routeBasedOnRole).toBe('function');
  });

  it('call a routeBasedOnRole, router push', async () => {

    const { vm } = wrapper;
    await vm.routeBasedOnRole();

    expect(mockRouter.push).toHaveBeenCalledTimes(1)

  });

  it('when the user is authenticated, response status is 200', async ()=>{

    //mock attemptLogin from LoginService, and make the response status 200
    const attemptLoginResponseStatus = 200
    axios.post.mockImplementation(() =>
      Promise.resolve({ status: attemptLoginResponseStatus })
    )
    // do the call
    const loginRequest = {username: "Kurl@mail.com", password: "1234"}
    const attemptLoginResponse = await LoginService.attemptLogin(loginRequest)

    //  check response
    expect(attemptLoginResponse.status).toEqual(attemptLoginResponseStatus)
  })

})

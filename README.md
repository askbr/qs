# Frivillig prosjekt i Fullstack, 2022
Prosjektet er en implementasjon av utdelt frivillig oppgave i Fullstack (IDATT2105)
våren 2022. Utført av: Ask Brandsnes Røsand og Moaaz Yanes ved bruk av Vue.js og Spring Boot
med JDBC mot database.

Prosjektoppgaven går ut på å lage en plattform hvor studenter kan godkjenne oppgaver
de har fått utdelt i ulike emner. Dette inkluderer å lage et kø-system som legger til
rette for at studenter kan si ifra til assistenter at de er klare for å godkjenne en 
gitt oppgave. Oppgavene blir rettet av student assistenter og emnene (inkludert oppgaver) 
blir opprettet av lærere.

Systemet kan brukes til å sette krav på hvor mange oppgaver en student må utføre i
et gitt emne for å kunne ta eksamen. Emner trenger ikke nødvendigvis å ha slike krav.

# Funksjonalitet
Foreløpig inneholder løsningen følgende funksjonalitet:
* **Admin (gjerne en lærer) har muligheten til å opprette/endre/slette et emne.** Hvert emne 
består av et fagnavn, unik fagkode, et antall oppgaver, et antall oppgaver nødvendig 
for å få godkjent i faget , liste med studenter og en liste med studentassistenter.


* **Opprette brukere og legge brukere (student eller assistent) til i fag.** Når brukere blir 
lagt til i et fag (dette gjøres ved opprettelse) vil brukeren automatisk bli opprettet dersom den ikke
eksisterer. 


* **Brukere har muligheten til stille seg i kø i et fag brukeren er en del av.** Når en bruker stiller
seg i kø har brukeren muligheten til å oppgi følgende informasjon: Om bruker ønsker hjelp eller godkjenning,
hvilken oppgave det omhandler og hvor brukeren befinner seg.


* **Brukere har muligheten til å se sin status i et fag.** Ulike fag kan ha ulike regler på hvor mange oppgaver
som må utføres. En student kan se sin status (godkjent fag/ ikke godkjent fag) på fagets side.


* **Assistenter har muligheten til å hjelpe studenter ved å plukke de i en kø over alle studenter.**
For å legge til rette for å ha flere studenter i et fag vil andre assistenter se at en student får hjelp.
Når en student får hjelp kan assistenten fjerne studenten fra køen og/eller godkjenne en oppgave for studenten.


* 


# Videre arbeid
* **Funksjonalitet for å åpne/lukke kø.** Foreløpig er kø-status en del av system men mangler betydning
og funksjonalitet for å forandre.


* **Funksjonalitet for å fjerne/legge til studenter i opprettet kø.**


* **Funksjonalitet for å opprette/slette studenter enkeltvis**.


* **Funksjonalitet for å arkivere emne.** Foreløpig er det kun mulig å slette et emne.


* **Email til brukere som blir opprettet.** Foreløpig er det ikke lagt til noen automatisk email-ordning
for å informere studenter om at de har fått opprettet en bruker i systemet.


* **Funksjonalitet for å søke etter emne.** Ettersom at applikasjonen potensielt kan ha et svært stort
antall fag bør det være mulig å søke etter et spesifikt fag. Spesielt for admins.


* **Contineous integration feiler backend-tester pga docker image ikke inneholder mySql database.**
Dette bør legges til, men er ikke blitt prioritert ettersom at tester kan kjøres ved bruk av IDEA.


* **Contineous deployment laster opp frontend og backend til ekstern server, men starter kun frotend.**
Dette bør endres slik at backend (database og spring REST API) automatisk oppdateres. Foreløpig må
denne delen av continous deployment gjøres manuelt.


* **Tette sikkerhetshull**
  * Foreløpig er det kun to roller som brukes på backend for autentisering: USER og ADMIN.
  Rollen studentassistent bør legges til for å forhindre brukere å manipulere hverandres køstatus
  ved bruk av token (antar at denne kan aksesseres fra frontend via vuex store). Også bør en rolle
  LÆRER legges til for å ikke gi lærere tilgang til alt.
  * Tokens blir opprettet ved hver innlogging. Dette fører til at det er flere tokens i omløp.
  En bedre løsning ville vært å legge til et lager av tokens, for å gjenbruke de.


* **Flere tester både på frontend og backend**. Foreløpig er det få tester både på frotend og backend,
dette har dessverre blitt nedprioritert til fordel for funksjonalitet. I videre arbeid ville blitt
høyt prioritert.


Dette er noe av mangelende funksjonalitet, det er høyest sannsynlig mer som bør legges til i lista.
Mye av dette kunne blitt oppdaget ved å utføre brukertester og teste produktet i praksis.

# Dokumentasjon
## API Dokumentasjon
API er dokumenter ved bruk av swagger. Lenke:
https://app.swaggerhub.com/apis-docs/askbr/qs-api/1.0.0

## Javadoc
Backend er dokumentert på javadoc format. 

## Modeller
### ER-Diagram (Database)
ER-diagram i UML (modell av databasen) kan finnes på lokasjonen `documentation/ER-Diagram.pdf`. 
(fra rotkatalogen til prosjektet)

# Installasjon
## Kjøre applikasjonen
### Steg 1: Oppsett av mySQL
1. Installer mysql lokalt på maskinen. Instrukser for installasjon kan finnes på 
https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/


2. Lag en ny database i sql ved navn `qs`. Dette kan gjøres ved å skrive
`CREATE DATABASE qs;` i mysql.


3. Opprett tabeller til applikasjonen ved å kjøre scriptet i filen 
`qs-backend/src/main/resources/db/db-setup.sql`. Scriptet inkluderer og opprettelse av ADMIN bruker
som senere kan brukes til innlogging.


### Steg 2: Oppsett av Spring Boot
1. `Java 11` kreves for backend i denne applikasjonen. Dette må installeres.


2. Installer maven for å kunne gjøre Spring Boot applikasjonen (backend). 
Dette kan gjøres fra https://maven.apache.org/. Versjon 3.8.4 fungerer garantert, men andre versjoner kan og fungere.


3. Gå til filen `qs-backend/src/main/resources/application.properties` og sett inn ditt brukernavn og passord
etter `=` i feltene: `spring.datasource.username` og `spring.datasource.username`.

### Steg 3: Oppsett av Node
1. Installer node.js. Versjon `16.14.0` fungerer garantert, men andre versjoner kan og fungere.


2. Installer npm. Versjon `8.5.1` fungerer garantert, men andre versjoner kan og fungere.


3. Installer quasar. Dette kan gjøres ved bruk av npm ved kommandoen: `npm install -g @quasar/cli`


4. Kjør kommandoen `npm install` fra mappen `qs-frontend`.

### Steg 4: Start applikasjonen
1. Gå til mappen `qs-backend` (fra rotkatalog) og skriv kommandoen: `mvn spring-boot:run`.


2. Gå til mappen `qs-frontend` (fra rotkatalog) og skriv kommandoen: `quasar dev`

Nå skal applikasjonen være oppe å kjøre og kan aksesseres i nettleser via adressen:
`localhost:8080`
## Kjøre tester
*Det antas at oppsett for kjøring av applikasjonen er gjennomført.* Her også kreves
mysql, node.js, maven, java 11, quasar.

### Kjøre tester backend
1. Lag en ny database i mysql ved navn `qstest`.


2. Sett opp databasen qstest ved å kjøre script `qs-backend/src/test/resources/sql/setup/setup-db.sql`


3. Skriv `mvn test` fra mappen `qs-backend` for å kjøre tester.


### Kjøre tester frontend
Skriv kommandoen `npm run test:unit` fra mappen `qs-frontend`